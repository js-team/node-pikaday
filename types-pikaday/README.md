# Installation
> `npm install --save @types/pikaday`

# Summary
This package contains type definitions for pikaday (https://github.com/dbushell/Pikaday).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pikaday.

### Additional Details
 * Last updated: Mon, 04 Jul 2022 12:02:21 GMT
 * Dependencies: [@types/moment](https://npmjs.com/package/@types/moment)
 * Global values: `Pikaday`

# Credits
These definitions were written by [Rudolph Gottesheim](https://github.com/MidnightDesign), [Åke Wivänge](https://github.com/wake42), and [Istvan Mezo](https://github.com/mezoistvan).
